#!/usr/bin/env python

from pylab import *

import acor
from argparse import ArgumentParser
from astropy.utils.console import ProgressBar
import celerite
import emcee
import h5py
import multiprocessing as multi
import scipy.signal as ss
import warnings

class LinePosterior(object):
    def __init__(self, ts, data, f0, bw):
        self._ts = ts
        self._data = data
        self._f0 = f0
        self._bw = bw

        self._mean = mean(data)
        self._rms = std(data)

        self._kernel = celerite.terms.SHOTerm(log(1.0), log(1000.0), log(2*pi*f0)) + \
                       celerite.terms.JitterTerm(log(1.0))
        self._gp = celerite.GP(self.kernel, mean=self.mean, fit_mean=True)

    @property
    def ts(self):
        return self._ts
    @property
    def data(self):
        return self._data
    @property
    def f0(self):
        return self._f0
    @property
    def bw(self):
        return self._bw
    @property
    def mean(self):
        return self._mean
    @property
    def rms(self):
        return self._rms
    @property
    def kernel(self):
        return self._kernel
    @property
    def gp(self):
        return self._gp
    @property
    def parameter_names(self):
        return self.gp.get_parameter_names()

    def log_prior(self, p):
        lp = 0.0
        
        # log-normal prior on omega0; 2-sigma range is bw
        log_o0 = self.gp.get_parameter('kernel:terms[0]:log_omega0')

        lp += -0.5*square((log_o0 - log(2*pi*self.f0))/(self.bw/self.f0/4.0))

        # p(Q) = exp(-Q/Qmax)
        Qmax = (self.ts[-1]-self.ts[0])*self.f0
        log_Q = self.gp.get_parameter('kernel:terms[0]:log_Q')
        lp += log_Q - exp(log_Q)/Qmax

        return lp

    def log_likelihood(self, p):
        pp = p.copy()
        pp[-1] = self.mean + p[-1]*self.rms

        self.gp.set_parameter_vector(pp)
        self.gp.compute(self.ts, yerr=0.0)

        return self.gp.log_likelihood(self.data)

    def __call__(self, p):
        lp = self.log_prior(p)

        if lp == np.NINF:
            return lp
        else:
            return self.log_likelihood(p) + lp

    def predict(self, p, ts):
        pp = p.copy()

        pp[-1] = self.mean + self.rms*p[-1]

        self.gp.set_parameter_vector(pp)
        
        self.gp.compute(self.ts, yerr=0.0)

        return self.gp.predict(self.data, ts, return_cov=False)

def parameter_guess(logpost, ts, data, N=1):
    srate = 1.0/(ts[1]-ts[0])
    fs, psd = ss.welch(data, fs=srate, nperseg=data.shape[0]/16)

    sel = abs(fs - logpost.f0) < logpost.bw/2.0

    imax = argmax(psd[sel])
    f0, f1, f2 = fs[sel][[imax-1, imax, imax+1]]
    p0, p1, p2 = log(psd[sel][[imax-1, imax, imax+1]])

    fmax = (f2*f2*(p0-p1) + f0*f0*(p1-p2) + f1*f1*(p2-p0))/(2.0*(f2*(p0-p1) + f0*(p1-p2) + f1*(p2-p0)))
    pddot = 2.0*((p1-p0)/(f0-f1) + (p1-p2)/(f1-f2))/(f2-f0)

    sigma = sqrt(-1.0/pddot)

    Q = 4.0*fmax/sigma

    sel_noline = sel & (abs(fs - fmax) > 2.0*sigma)
    
    rms = sqrt(sum((fs[1]-fs[0])*psd[sel_noline]))

    p0 = array([log(1.0), log(Q), log(2*pi*fmax), log(rms), 0.0])
    logpost.gp.set_parameter_vector(p0)
    S0_factor = 2.0*pi*psd[sel][imax]/logpost.gp.kernel.get_psd(2.0*pi*array([fs[sel][imax], fs[sel][imax]]))[0]

    p0[0] = log(S0_factor)

    Nsamp = logpost.ts.shape[0]
    Nosc = (ts[-1]-ts[0])*fmax
    Nwander = Nosc/Q

    sigmas = array([1.0/sqrt(Nosc), 1.0/sqrt(Nwander), 1.0/sqrt(Nosc), 1.0/sqrt(Nsamp), 1.0/sqrt(Nsamp)])/100.0

    if N == 1:
        return p0 + sigmas*randn(sigmas.shape[0])
    else:
        return p0 + sigmas*randn(N, sigmas.shape[0])    

class PredictorSum(object):
    def __init__(self, logpost, ts):
        self._logpost = logpost
        self._ts = ts

    @property
    def logpost(self):
        return self._logpost
    @property
    def ts(self):
        return self._ts

    def __call__(self, *ps):
        pred = 0
        for p in ps:
            pred = pred + self.logpost.predict(p, self.ts)

        return pred
    
if __name__ == '__main__':
    parser = ArgumentParser(description='regress out a narrow line from the data')

    file_group = parser.add_argument_group('File Options')
    spectrum_group = parser.add_argument_group('Spectrum Options')
    sampler_group = parser.add_argument_group('Sampler Options')
    
    file_group.add_argument('--input-file', metavar='FILE', required=True, help='input HDF5 file')
    file_group.add_argument('--output-file', metavar='FILE', required=True, help='output HDF5 file (can be same as input)')
    file_group.add_argument('--input-channel', metavar='CHANNEL/NAME', required=True, help='name of input data channel')
    file_group.add_argument('--output-channel', metavar='CHANNEL/NAME', required=True, help='name of cleaned data channel')
    file_group.add_argument('--prediction-channel', metavar='CHANNEL/NAME', help='if present, write line prediction to this channel')
    file_group.add_argument('--overwrite', action='store_true', help='overwrite existing cleaned data if present')

    spectrum_group.add_argument('--linefreq', required=True, type=float, metavar='F', help='line frequency (cycles/time)')
    spectrum_group.add_argument('--srate', type=float, default=4096, metavar='SRATE',
                                help='sample rate (default %(default)s samples/time)')
    spectrum_group.add_argument('--bandwidth', type=float, default=16, metavar='BW',
                                help='notch filter bandwidth (default %(default)s cycles/time)')

    sampler_group.add_argument('--nwalkers', type=int, default=32, metavar='N', help='number of walkers (default %(default)s)')
    sampler_group.add_argument('--nsamples', type=int, default=1024, metavar='N', help='number of independent samples (default %(default)s)')
    sampler_group.add_argument('--sample-channel', metavar='CHANNEL/NAME', help='if given, store samples in this output file channel')
    sampler_group.add_argument('--nthreads', type=int, default=1, metavar='N', help='number of threads for sampler (default %(default)s)')

    args = parser.parse_args()

    print('Reading data')
    with h5py.File(args.input_file, 'r') as inp:
        data = array(inp[args.input_channel])
    dt = 1.0/args.srate
    ts = linspace(0, (data.shape[0]-1)*dt, data.shape[0])

    print('Decimating data')
    fny = args.srate/2.0
    flow = args.linefreq - args.bandwidth/2.0
    fhigh = args.linefreq + args.bandwidth/2.0

    bb, aa = ss.butter(4, [flow/fny, fhigh/fny], btype='bandpass', output='ba')
    filtered_data = ss.filtfilt(bb, aa, data)

    dec_factor = fny / args.bandwidth
    dec_factor_int = int(round(dec_factor))

    if abs(dec_factor - dec_factor_int) > 1e-8*dec_factor:
        warnings.warn('bandwidth implies decimation by non-integer factor, rounding to int')

    decimated_ts = ts[::dec_factor_int]
    decimated_data = filtered_data[::dec_factor_int]

    print('Setting up sampler')
    logpost = LinePosterior(decimated_ts, decimated_data, args.linefreq, args.bandwidth)

    ndim = 5

    ps = parameter_guess(logpost, ts, data, N=args.nwalkers)
    sampler = emcee.EnsembleSampler(args.nwalkers, ndim, logpost, threads=args.nthreads)

    expansion_factor = 16
    
    nsamp = int(round(args.nsamples/args.nwalkers))
    thin = 1
    while True:
        print('Sampling {:d} steps (thinning by {:d})'.format(nsamp, thin))
        with ProgressBar(nsamp) as bar:
            for ps, lnprobs, rstate in sampler.sample(ps, iterations=nsamp, thin=thin, storechain=True):
                bar.update()

        
        try:
            acls = sampler.get_autocorr_time()
            max_acl = np.max(acls)
            print('found max acl = {:.2g} (thinned) or {:.2g} (unthinned) in dimension {:d}'.format(max_acl, max_acl*thin, argmax(acls)))
        except emcee.autocorr.AutocorrError as x:
            print('could not compute ACLs yet: {:s}'.format(str(x)))
            max_acl = np.inf
            
        nsamples = sampler.flatchain.shape[0]/max_acl

        if nsamples >= args.nsamples:
            print('got enough samples, done')
            break
        else:
            print('not enough samples yet, doubling')
            ps = sampler.chain[:,-1,:] # Save the final iteration of the chain into ps
            nsamp = nsamp*2
            if nsamp <= expansion_factor*args.nsamples/args.nwalkers or nsamp <= 256:
                thin = 1
            else:
                thin *= 2
            sampler.reset()

    
    print('Computing mean prediction')
    samps = sampler.flatchain[np.random.choice(sampler.flatchain.shape[0], size=args.nsamples, replace=False), :]
    mean_prediction = zeros_like(data)
    if args.nthreads == 1:
        with ProgressBar(args.nsamples) as bar:
            for s in samps:
                mean_prediction = mean_prediction + logpost.predict(s, ts)
                bar.update()
    else:
        predictor_sum = PredictorSum(logpost, ts)
        with ProgressBar(args.nsamples) as bar:
            pool = multi.Pool(args.nthreads)
            try:
                csize = max(1, int(round(samps.shape[0]/(4.0*args.nthreads))))
                i = 0
                while i < samps.shape[0]:
                    inext = min(i+csize, samps.shape[0])
                    ntodo = inext - i

                    def cb(pred_sum):
                        global mean_prediction
                        mean_prediction = mean_prediction + pred_sum
                        for j in range(ntodo):
                            bar.update()

                    pool.apply_async(predictor_sum, samps[i:inext,:], {}, cb)
                    i = inext
            finally:
                pool.close()
                pool.join()
    mean_prediction = mean_prediction / args.nsamples
    
    print('Writing data')
    with h5py.File(args.output_file, 'a') as out:
        if args.overwrite:
            try:
                del out[args.output_channel]
            except:
                pass
        out.create_dataset(args.output_channel, data=data-mean_prediction, shuffle=True, compression='gzip')

        if args.prediction_channel is not None:
            if args.overwrite:
                try:
                    del out[args.prediction_channel]
                except:
                    pass
            out.create_dataset(args.prediction_channel, data=mean_prediction, shuffle=True, compression='gzip')

        if args.sample_channel is not None:
            if args.overwrite:
                try:
                    del out[args.sample_channel]
                except:
                    pass
            out.create_dataset(args.sample_channel, data=sampler.flatchain, shuffle=True, compression='gzip')
