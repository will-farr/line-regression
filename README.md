A notebook that uses time-domain stochastic process models (CARMA) implemented 
in the [celerite](http://celerite.readthedocs.io/en/stable/) package to regress
out lines in LIGO data.

You will need to have installed:

* The numpy/scipy/matplotlib stack.

* [acor](https://pypi.python.org/pypi/acor) to estimate
  autocorrelation lengths for convergence tests.

* [celerite](http://celerite.readthedocs.io/en/stable/) which provides
  the Kalman filter (actually, it uses a clever matrix trick involving
  a dimensional expansion and Cholesky decomposition) that tracks the
  line.

* [corner](https://github.com/dfm/corner.py) to make the corner plots
  of posteriors.

* [emcee](http://dan.iel.fm/emcee/current/) for fitting the parameters of the line.

* [h5py](http://docs.h5py.org/en/latest/index.html) to read and write
  HDF5 files that store the LOSC data and the line-subtracted outputs.

* [Seaborn](https://seaborn.pydata.org/) for pretty plots.